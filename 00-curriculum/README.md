# Chương trình huấn luyện React Native thực chiến tại GenbaCoder

<img src="./logo.png" width="100px"/>

## Mục tiêu của Khóa huấn luyện
- Sử dụng React Native để xây dựng ứng dụng di động thực tế
- Thành thục Redux
- Triển ứng dụng di động của riêng mình lên App Store, Google Play Stores

## Đối tượng tham gia
- Bạn là lập trình viên Web Frontend và muốn bổ sung kỹ năng phát triển ứng dụng di động 
- Bạn là lập trình viên Backend và muốn trở thành lập trình viên fullstack
- Bạn đã tham gia những khóa học React Native trước đây và thấy chương trình học không được như mong muốn của bạn vì chất lượng và mất động lực trong khi học
- Bàn là sinh viên và muốn nâng cao kỹ năng để chuẩn bị bước vào con đường trở thành lập trình viên

### Tools
- Máy tính cá nhân (Linux, Mac, Windows)

### Mindset
- Học hỏi, thực hành và cải tiến liên tục
- Tò mò thí nghiệm công nghệ mới
- Thực chiến với những case study thực tế
- Đọc source code của React Native là một niềm vui

## Chương trình chi tiết

### Tuần 1
- Code Walkthrough: cài đặt môi trường phát triển, `JSX`, `View`, `Component`, `Event`, `Style`, `Layout`
- Code Lab: chương trình kiểm tra số nguyên tố
- Code Practice: chương trình đồng hồ đếm ngược

### Tuần 2
- Code Walkthrough: `FlatList`, `ScrollView`, `TabView`, `State`, `Props`, `Lifecycle`
- Code Lab: ứng dụng Social (giao diện cơ bản, màn hình User, Post)
- Code Practice: chương trình máy tính cầm tay

### Tuần 3
- Code Walkthrough: `Custom View`, `HTTP request`, `Error Handling`, `Navigation`, `Debug`
- Code Lab: ứng dụng Social (hiển thị data, ráp flow navigate)
- Code Practice: Coinmarketcap clone (giao diện cơ bản, hiện thị giá thị trường cypto)

### Tuần 4
- Code Walkthrough: `redux`, `redux thunk`, các thư viện thường dùng (`picker,` `keyboard`)
- Code Lab: ứng dụng Social (sử dụng redux, tích hợp navigation vào redux)
- Code Practice: Coinmarketcap clone (chức năng: Favorite, Search, Filter, Details)

### Tuần 5
- Code Walkthrough: `AsyncStorage`, `Cache image`, `Realm Database`, `Firebase`,`Socket IO`
- Code Lab: ứng dụng Social(chức năng History)
- Code Practice: Coinmarketcap clone (chức năng: Realtime, đồ thị giá, tin tức về Crypto)

### Tuần 6
- Code Walkthrough: `Animation`, `Webview`, quy trình làm ứng dựng
- Code Lab:
- Code Practice: Coinmarketcap clone (chức năng: login với Facebook, Chat, chia sẻ thông tin)

### Tuần 7
- Code Walkthrough: tối ưu ứng dụng, communication giữa React Native và Native
- Xây dựng dự án cá nhân
### Tuần 8
- Xây dựng dự án cá nhân (cont.)
- DEMO Day