import React, {Component} from 'react';
import {
    TouchableOpacity,
    StyleSheet,
    Text,
    Platform
} from 'react-native';

export default class Button extends Component {

    render() {

        const isAndroid = Platform.OS === 'android';

        const {children, onButtonClick, style, textStyle, count} = this.props;
        const c = count ? ': ' + count : '';
        return (
            <TouchableOpacity
                style={[styles.button, style]}
                onPress={() => onButtonClick && onButtonClick(children)}
            >
                <Text style={textStyle}>{children}{c}</Text>
            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({
    button: {
        width: '50%',
        height: 60,
        borderRadius: 5,
        borderColor: '#cc76b7',
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    }
});

