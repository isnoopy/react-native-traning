import React, {Component} from 'react';
import {
    StyleSheet, Text,
} from 'react-native';
import {FONT_BOLD} from "./fonts/fonts";

export default class CustomText extends Component {

    render() {
        return (
            <Text
                style={{
                    fontSize: 20,
                    fontFamily: FONT_BOLD
                }}
            >Hello GenbaCoder</Text>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

