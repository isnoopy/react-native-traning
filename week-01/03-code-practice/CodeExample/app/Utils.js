export default class Utils {
    static upperCaseName(name) {
        return name.toUpperCase();
    }
}

export const upperCaseName = (name) => {
    return name.toUpperCase();
};