import React, {Component} from "react";
import {View, StyleSheet, Text} from 'react-native';
import {PRIMARY, SECONDARY} from "./res/colors";
import {buttons} from "./app/buttons";
import Button from "./app/component/Button";

type Props = {};
export default class App extends Component<Props> {

    constructor(props) {
        super(props);
        this.state = {
            previousInputValue: 0,
            inputValue: 0,
            selectedSymbol: null
        }
    }

    render() {
        const {inputValue} = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.container_result}>
                    <Text style={styles.text_result}>{inputValue}</Text>
                </View>
                <View style={styles.container_action}>
                    {this.renderButton()}
                </View>
            </View>
        );
    }

    renderButton = () => {
        return buttons.map((row, index) => {
            return (
                <View key={'row' + index} style={styles.view_row}>
                    {row.map(button => {
                        return (
                            <Button
                                onPress={this.onAction}
                                key={button.name}
                                attr={button}
                            />
                        );
                    })}
                </View>
            );
        })
    };

    onAction = (action) => {

    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    container_result: {
        flex: 3,
        backgroundColor: PRIMARY
    },
    container_action: {
        flex: 7,
        backgroundColor: SECONDARY
    },
    view_row: {
        flex: 1,
        flexDirection: 'row'
    },
    text_result: {
        fontSize: 80,
        position: 'absolute',
        right: 20,
        bottom: 15,
        color: '#ffffff'
    }
});
