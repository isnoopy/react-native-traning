import {DARK, HIGHLIGHT} from "../res/colors";

export const buttons = [
    [{name: 'C', color: DARK}, {name: '+/-', color: DARK}, {name: '%', color: DARK}, {name: '/', color: HIGHLIGHT}],
    [{name: '7'}, {name: '8'}, {name: '9'}, {name: '*', color: HIGHLIGHT}],
    [{name: '4'}, {name: '5'}, {name: '6'}, {name: '-', color: HIGHLIGHT}],
    [{name: '1'}, {name: '2'}, {name: '3'}, {name: '+', color: HIGHLIGHT}],
    [{name: '0', percent: 2}, {name: '.'}, {name: '=', color: HIGHLIGHT}],
];