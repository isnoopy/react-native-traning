import React, {Component} from 'react';
import {
    StyleSheet, TouchableHighlight, Text, View
} from 'react-native';
import {PRIMARY, SECONDARY} from "../../res/colors";

export default class Button extends Component {

    render() {
        const {name, color, percent} = this.props.attr;
        const {onPress} = this.props;
        const backgroundColor = color ? color : SECONDARY;
        const flex = percent ? percent : 1;
        return (
            <TouchableHighlight
                style={[styles.container, {flex}]}
                onPress={() => {
                    onPress && onPress(name);
                }}
                underlayColor={'#cadbdf'}
            >
                <View style={[styles.view_border, {backgroundColor}]}>
                    <Text style={styles.text}>{name}</Text>
                </View>
            </TouchableHighlight>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    view_border: {
        flex: 1,
        width: '100%',
        borderRadius: 0,
        borderColor: PRIMARY,
        borderWidth: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: '#ffffff',
        fontSize: 30
    },
});

