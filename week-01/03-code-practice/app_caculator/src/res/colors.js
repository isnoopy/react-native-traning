export const PRIMARY = '#2c2d2f';
export const DARK = '#404143';
export const SECONDARY = '#5f6062';
export const HIGHLIGHT = '#fd9e2b';
