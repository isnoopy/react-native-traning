import React, {Component, createRef} from 'react';
import {StyleSheet, RefreshControl, View, ScrollView, Text} from 'react-native';
import ListComponent from "./ListComponent";

type Props = {};
export default class App extends Component<Props> {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false
        };
    }


    render() {
        return (
            <ListComponent/>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
