import React, {Component} from 'react';
import {
    View,
    StyleSheet, Text,
} from 'react-native';

export default class ItemView extends Component {

    render() {
        const {item, index} = this.props;
        return (
            <View style={{height: 70, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{
                    fontSize: 20
                }}>Name: {item.name}</Text>
                <Text style={{
                    fontSize: 30
                }}>Index: {index}</Text>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

