import React, {Component} from 'react';
import {
    View,
    StyleSheet, FlatList, Text, TouchableOpacity
} from 'react-native';
import ItemView from "./ItemView";

export default class ListComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            data: [],
            index: 1
        }
    }

    componentDidMount() {
        this.setState({data: this.getData()});
    }

    getData = () => {
        let data = [];
        for (let i = 0; i < 20; i++) {
            data.push({name: this.state.index + ' item data ' + i});
        }
        this.setState({index: this.state.index + 1});
        return data;
    };

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    ref={r => this.list = r}
                    style={{marginTop: 40}}
                    // horizontal={true}
                    // numColumns={3}
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}
                    data={this.state.data}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                    ListHeaderComponent={this.renderHeader}
                    onEndReached={this.onEndReached}
                    onEndReachedThreshold={0.1}
                />
            </View>
        );
    }

    onEndReached = () => {
        setTimeout(() => {
            let data = this.getData();
            this.setState({data: [...this.state.data, ...data]});
        }, 1000);
    };

    onRefresh = () => {
        //Pull to Refresh
        this.setState({refreshing: true});
        setTimeout(() => {
            this.setState({refreshing: false, index: 1}, () => {
                this.setState({data: this.getData()});
            });

        }, 3000);
    };
    renderHeader = () => {
        return (
            <TouchableOpacity
                onPress={this.scrollToEnd}
                style={{height: 150, backgroundColor: '#cb7bcc', justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{
                    fontSize: 20
                }}>Header List</Text>
            </TouchableOpacity>
        );
    };

    scrollToEnd = () => {
        // this.list.scrollToEnd();
        this.list.scrollToIndex({index: 15});
    };

    _keyExtractor = (it, index) => {
        return it.name;
    };

    _renderItem = ({item, index}) => {
        return (
            <ItemView item={item} index={index}/>
        );
    };
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

