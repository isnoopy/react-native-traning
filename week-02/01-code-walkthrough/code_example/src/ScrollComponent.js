import React, {Component, createRef} from 'react';
import {StyleSheet, RefreshControl, View, ScrollView, Text} from 'react-native';

type Props = {};
export default class ScrollComponent extends Component<Props> {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false
        };
    }

    getArray = () => {
        let array = [];
        for (let i = 0; i < 20; i++) {
            array.push('item ' + i);
        }
        return array;
    };

    _onRefresh = () => {
        this.setState({refreshing: true});
        setTimeout(() => {
            this.setState({refreshing: false});
        }, 3000);
    };

    render() {
        return (
            <ScrollView
                ref={this.getRef}
                style={{flex: 1}}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                    />
                }
                stickyHeaderIndices={[0, 5]}
            >
                <View style={styles.container}>
                    {this.renderItem('item 1')}
                    {this.renderItem('item 2')}
                    {this.renderItem('item 3')}
                    {this.renderItem('item 4')}
                    {this.renderItem('item 5')}
                    {this.renderItem('item 6')}
                    {this.renderItem('item 7')}
                    {this.renderItem('item 8')}
                    {this.renderItem('item 9')}
                    {this.renderItem('item 10')}
                    {this.renderItem('item 11')}
                    {this.renderItem('item 12')}
                    {this.renderItem('item 13')}
                    {this.renderItem('item 14')}

                </View>
            </ScrollView>
        );
    }

    getRef = (ref) => {
        this.scrollView = ref;
    };

    scrollToEnd = () => {
        this.scrollView.scrollToEnd();
    };

    renderItem = (value) => {
        return (
            <Text
                key={value}
                style={{
                    width: '100%',
                    height: 100,
                    fontSize: 30,
                    alignSelf: 'center'
                }}
            >{value}</Text>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
