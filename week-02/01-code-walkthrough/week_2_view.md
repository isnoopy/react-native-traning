# View & Event

### Roadmap
 - Layout và Style trong React native.
    - Flex-box
    - Absolute
 - FlatList
 - ScrollView

#### 1. Layout và style

 - Flex-box: `flexDirection` = [`row`,`column`] *(default=`column`)*
 - Đặt tỷ lệ cho layout: `flex`
 - Absolute: `position: absolute`, `left`, `right`, `top`, `bottom`

#### 2. ScrollView

 - Refresh control.
 - Event onScroll.

#### 3. FlatList

 - Display list.
 - Pull to refresh.
 - Load more.
 - Header and Footer.
 - Grid view.
 - Event onScroll.


