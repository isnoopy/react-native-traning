# Lab week 2

### RoadMap

 - Làm giao diện Ứng dụng Social đơn giản.
    + Làm giao diện màn hình List User.
    + Làm giao diện màn hình List Posts.

#### 1. Chức năng trong ứng dụng

 - Hiển thị danh sách Posts.
 - Hiển thị danh sách User.
 - Hiển thị detail Post, danh sách comments của Post.
 - Hiển thị detail User.
 - Lưu Post vào history.
#### 2. Code

 - Init project:
```sh
react-native init GenbaCoddLab
```

```sh
cd genba-code-lab
```

```sh
mkdir app
```

 - `App.js`:

 <img src="./image/img-app.png" width="600px"/>

 - UI cho màn hình list Users:

 <img src="./image/img-list-user.png" width="300px"/>

 - UI cho màn hình list Posts:

 <img src="./image/img-list-post.png" width="300px"/>

 - UI cho màn hình detail User:

 <img src="./image/img-detail-user.png" width="300px"/>

 - UI cho màn hình detail Post:

 - Style shadow:

 ```js
 style:{
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.2,
    elevation: 1,
 }
 ```