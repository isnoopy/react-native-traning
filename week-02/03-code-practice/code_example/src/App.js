import React, {Component} from 'react';
import {MainRouter} from "./app/router/MainRouter";


type Props = {};
export default class App extends Component<Props> {

    render() {
        return (
            <MainRouter/>
        );
    }
}
