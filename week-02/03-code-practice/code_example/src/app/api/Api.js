const API_URL = '';

const getParams = (method: string, data: any, token = null) => {
    return {
        method: method,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            authentication: token
        },
        body: JSON.stringify(data)
    };
};

export const request = async (endpoint: string, method: string, body) => {
    const token = '';
    const fullEndpoint = API_URL + endpoint;
    return fetch(fullEndpoint, getParams(method, body, token))
        .then(res => {
            try {
                return res.json();
            } catch (e) {
                throw e;
            }
        })
        .then(data => handleError(data))
        .catch(error => {
            throw error;
        });
};

const handleError = ({error, data}) => {
    if (error) {
        throw {
            message: error.message,
            code: error.code
        };
    } else {
        return data;
    }
};

export const login = ({username, password}) => {
    return request('/login', 'POST', {username, password});
};

