import React, {Component} from 'react';
import {
    View,
    StyleSheet, Image,
} from 'react-native';
import PropTypes from 'prop-types';

import {sizeWidth} from "../utils/Size";

export default class MenuTabItem extends Component {
    render() {
        const {icon, active} = this.props;
        const tintColor = active ? '#8b1c10' : '#2EFECF';
        return (
            <View style={styles.item}>
                <Image resizeMode={'contain'} style={[styles.icon, {tintColor}]} source={icon}/>
            </View>
        );
    }
}

MenuTabItem.propTypes = {
    icon: PropTypes.any,
};

const styles = StyleSheet.create({
    icon: {
        width: sizeWidth(7.2),
        height: sizeWidth(7.2),
    },
    item: {
        justifyContent: 'flex-end',
        marginTop: sizeWidth(2)
    }
});
