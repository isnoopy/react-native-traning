import React from "react";
import {createStackNavigator} from "react-navigation";
import {TabRouter} from "./TabRouter";
import DetailScreen from "../screen/DetailScreen";

export const MainRouter = createStackNavigator({
    Main: {
        screen: TabRouter,
        navigationOptions: {
            header: null
        }
    },
    Detail:{
        screen: DetailScreen
    }
});
