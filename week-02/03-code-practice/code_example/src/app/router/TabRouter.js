import React from "react";
import {createBottomTabNavigator} from "react-navigation";
import CoinScreen from "../screen/CoinScreen";
import SettingScreen from "../screen/SettingScreen";
import MenuTabItem from "../component/MenuTabItem";
import ic_coin from '../../res/img/ic_coin.png';
import ic_setting from '../../res/img/ic_setting.png';

export const TabRouter = createBottomTabNavigator({
    CoinList: {
        screen: CoinScreen,
        navigationOptions: {
            title: 'CoinList',
            tabBarLabel: 'List',
            tabBarIcon: ({focused}) => {
                return <MenuTabItem
                    icon={ic_coin}
                    active={focused}
                />
            }
        }
    },
    Setting: {
        screen: SettingScreen,
        navigationOptions: {
            tabBarLabel: 'Setting',
            tabBarIcon: ({focused}) => {
                return <MenuTabItem
                    icon={ic_setting}
                    active={focused}
                />
            }
        }
    }
}, {
    // tabBarComponent: TabBarComponent,
    tabBarPosition: 'bottom',
    initialRouteName: 'CoinList',
    tabBarOptions: {
        indicatorStyle: {
            backgroundColor: 'transparent'
        },
        showIcon: true,
        showLabel: false,
        tabStyle: {
            alignItems: 'center',
        }
    }
});

