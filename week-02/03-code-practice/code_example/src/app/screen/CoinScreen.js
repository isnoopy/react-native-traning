import React, {Component} from 'react';
import {
    View,
    StyleSheet, Text,
} from 'react-native';

export default class CoinScreen extends Component {


    render() {
        return (
            <View style={styles.container}>
                <Text style={{fontSize: 30}}>Coin Screen</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

