# React Native Practice Week 3

<img src="./image/logo.png" width="100px"/>

## Ứng dụng theo dõi giá Crypto Currency

- [ ] Tạo giao điện ứng dụng theo dõi giá Crypto Currency theo Mockup
- [ ] Lấy data từ API hiển thị lên giao diện.
- [ ] Thiết kế trang Detail cho từng coin.

## Public API

`https://api.coinmarketcap.com/v2/ticker/?limit=10`

## Mockup theo dõi giá Crypto Currency

<img src="./image/CryptoWatcher-01.PNG" width="400px"/>
