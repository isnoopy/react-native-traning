# Day 4

### Roadmap
 - Redux
    - Giới thiệu về redux
    - Store
    - Action
    - Reducer
 - Redux thunk
    - Middleware
    - Dispatch action
 - Những thư viện thường được sử dụng trong dự án
    - Datetime picker
    - MomentJs
    - Keyboard
    - Firebase

#### 1. Redux

 - Redux Flow:
 
<img src="./image/ReduxFlow.png" width="600px"/>

 - Demo App:

<img src="./image/img_redux_simple.gif" width="300px"/>

 - Setup:

```sh
    npm install --save redux react-redux
```

<img src="./image/img-redux-simple-folder.png" width="300px"/>

 - `store` && `reducers`:

<img src="./image/img-reducer-store.png" width="600px"/>

 - `action`:

<img src="./image/img-action.png" width="600px"/>

 - `Provider`:

<img src="./image/img-action.png" width="600px"/>

 - Register `Provider`:
 
<img src="./image/img-app-provider.png" width="600px"/>

 - Implement:

<img src="./image/img_screen.png" width="300px"/>

 + `SimpleDemo.js`:

<img src="./image/img-simple-demo.png" width="600px"/>

 + `DisplayView.js`:

<img src="./image/img-display-view.png" width="600px"/>

 + `InputView.js`:

<img src="./image/img-imput-view.png" width="600px"/>

 + `Button.js`:

<img src="./image/img-button.png" width="600px"/>

#### 2. Redux thunk

<img src="./image/img_redux_thunk.gif" width="300px"/>

 - Setup:

 ```sh
 npm install --save redux-thunk
 ```
 - api handle:

 <img src="./image/img-api.png" width="600px"/>

 - `store` & `reducer`:

<img src="./image/img-thunk-reducer.png" width="600px"/>

 - `action`:

 <img src="./image/img-thunk-action.png" width="600px"/>

 - Implement:

 <img src="./image/img-redux-thunk-demo.png" width="600px"/>