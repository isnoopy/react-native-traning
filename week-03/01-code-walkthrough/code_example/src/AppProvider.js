import React, {Component} from "react";
import {Provider} from "react-redux";
import App from "../../App";

import {thunkStore} from "./walkthrough/thunk/ThunkReducer";

export default class AppProvider extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Provider store={thunkStore}>
                <App/>
            </Provider>
        );
    }
}

