import React, {Component} from 'react';
import {
    View,
    StyleSheet, Text
} from 'react-native';


import {connect} from "react-redux";

class DisplayView extends Component {

    constructor(props) {
        super(props);
    }

    /**
     * Use store value: this.props.index
     * @returns {*}
     */
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Value: {this.props.index}</Text>
            </View>
        );
    }
}

/**
 * Chuyen gia tri tu store sang props cua component
 * @param state ~ store
 * @returns {{index: *}} ~ this.props.index
 */
const mapState2Props = (state) => {
    return {
        index: state.value
    }
};
/**
 * Chuyen gia tri action vao props
 * @type {{}}
 */
const mapAction2Props = {};

/**
 * connect(mapState, mapAction)(NameOfComponent)
 */
export default connect(mapState2Props, mapAction2Props)(DisplayView);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 40,
        color: '#797739'
    }
});
