import React, {Component} from 'react';
import {
    View,
    StyleSheet, TextInput, TouchableOpacity, Text
} from 'react-native';


import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {setValue} from "../Action";

class InputView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    ref={r => this.input = r}
                    style={styles.input_style}
                    placeholder={'Input value'}
                    onChangeText={this.onValueChange}
                />
                <TouchableOpacity
                    style={styles.button_style}
                    onPress={this.onPress}
                >
                    <Text style={styles.text_button}>SET VALUE</Text>
                </TouchableOpacity>
            </View>
        );
    }
    onPress = () => {
        if (this.state.value) {
            this.props.setValue(this.state.value);
            this.input.clear();
        }
    };
    onValueChange = (text) => {
        try {
            let value = parseInt(text);
            this.setState({value});
        } catch (e) {
            console.log(e);
        }
    };
}

const mapAction2Props = (dispatch) => {
    return bindActionCreators({setValue}, dispatch);
};
export default connect(null, mapAction2Props)(InputView);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    button_style: {
        width: '60%',
        height: 50,
        backgroundColor: '#797739',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input_style: {
        width: '60%',
        height: 50,
        textAlign: 'center',
        fontSize: 20,
        backgroundColor: '#ffffff',
        color: '#797739'
    },
    text_button: {
        fontSize: 20,
        color: '#ffffff'
    }
});



