# React Native Practice Week 4

<img src="./image/logo.png" width="100px"/>

## Ứng dụng theo dõi giá Crypto Currency (tiếp theo)

- [ ] Thêm tính năng xem chi tiết đồng Cryto khi được chọn.
- [ ] Sử dụng redux cho chức năng get thông tin ở màn hình list coin.
- [ ] Xây dựng tính năng đọc tin tức về Crypto được lấy từ rss

## RSS tin tức ví dụ:

`https://cointelegraph.com/rss-feeds`

## Mockup demo phần tin tức:

<img src="./image/news.png" width="300px"/>