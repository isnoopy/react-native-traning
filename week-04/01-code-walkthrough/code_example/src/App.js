import React, {Component} from 'react';
import ReduxDemo from "./screen/ReduxDemo";
import LoadingView from "./components/LoadingView";
import {View} from 'react-native';

type Props = {};

export default class App extends Component<Props> {
    render() {
        return (
            <View style={{flex: 1, justifyContent: 'center'}}>
                <LoadingView/>
                <ReduxDemo/>
            </View>
        );
    }
}

