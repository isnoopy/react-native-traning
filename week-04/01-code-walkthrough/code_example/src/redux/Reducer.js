import {applyMiddleware, combineReducers, createStore} from 'redux';
import {coinReducer} from "./coin/CoinReducer";
import {themeReducer} from "./theme/ThemeReducer";
import thunk from "redux-thunk";

/**
 * store after combine
 * @type {{coin: {loading: boolean, data: Array, error: null}, theme: {value: number, theme: string}}}
 */
const stateDefault = {
    coin: {
        loading: false,
        data: [],
        error: null
    },
    theme: {
        value: 0,
        theme: 'light'
    }
};
const reducer = combineReducers({
    coin: coinReducer,
    theme: themeReducer
});

export const store = createStore(reducer, applyMiddleware(thunk));
