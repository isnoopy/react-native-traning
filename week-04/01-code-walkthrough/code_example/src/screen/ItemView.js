import React, {Component} from 'react';
import {
    View,
    StyleSheet, Text,
} from 'react-native';
import {connect} from 'react-redux';
import {DARK, LIGHT} from "../res/colors";

class ItemView extends Component {

    render() {
        const {item, theme} = this.props;
        const color = theme === 'light' ? LIGHT : DARK;
        const name = item && item.name || '';
        const price_usd = item && item.price_usd || 0;
        return (
            <View
                style={styles.item_container}
            >
                <Text style={[styles.item_text, {color}]}>{name} - {price_usd}</Text>
            </View>
        );
    }
}

const mapState2Props = (state) => {
    return {
        theme: state.theme.theme
    }
};
export default connect(mapState2Props)(ItemView);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    item_container: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        flexDirection: 'row'
    },
    item_text: {
        fontSize: 20,
        marginLeft: 20,
    }
});

