# Day 4

### Roadmap
 
 - Redux thunk
    - Middleware
    - Dispatch action
 - Những thư viện thường được sử dụng trong dự án
    - Datetime picker
    - MomentJs
    - Keyboard


####  Redux thunk

<img src="./image/img_redux_thunk.gif" width="300px"/>

 - Setup:

 ```sh
 npm install --save redux-thunk
 ```
 - api handle:

 <img src="./image/img-api.png" width="600px"/>

 - `store` & `reducer`:

<img src="./image/img-thunk-reducer.png" width="600px"/>

 - `action`:

 <img src="./image/img-thunk-action.png" width="600px"/>

 - Implement:

 <img src="./image/img-redux-thunk-demo.png" width="600px"/>