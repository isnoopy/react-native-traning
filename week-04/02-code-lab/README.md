# Week 4

### RoadMap

 - Thêm redux vào dự án. Tạo action, quản lý fetch data bằng redux.
 - Thêm chức năng favourite và thay đổi trong màn hình detail, cập nhật phía ngoài danh sách.
 - Tích hợp react-navigation vào redux.

#### 1. Redux

 - Progressing View

<img src="./image/img-loadview.png" width="600px"/>

 - `App.js` with LoadView

<img src="./image/img-app-with-loadview.png" width="600px"/>

 - Init loading state

<img src="./image/img-loading-reducer.png" width="600px"/>

 - Loading Action

<img src="./image/img-loading-action.png" width="600px"/>

#### 2. React-navigation với redux

 - `navigationMiddleware` & `reduxifyNavigator`

<img src="./image/img-navigation-middleware.png" width="600px"/>

 - Init state và navigate reducer:

<img src="./image/img-nav-state.png" width="600px"/>

 - `pushScreen`, `popScreen`, `resetPage`:

<img src="./image/img-push-pop.png" width="600px"/>

 - Set params

<img src="./image/img-set-param.png" width="600px"/>

 - Get params in component

<img src="./image/img-get-param.png" width="600px"/>

