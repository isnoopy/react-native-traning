import {createStackNavigator} from 'react-navigation';
import ReduxDemo from "../screen/ReduxDemo";
import DetailScreen from "../screen/detail/DetailScreen";

export const MainRouter = createStackNavigator({
    Main: {
        screen: ReduxDemo
    },
    Detail: {
        screen: DetailScreen
    }
});