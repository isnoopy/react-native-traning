import {NavigationActions} from "react-navigation";
import {MainRouter} from "./MainRouter";

const initialState = MainRouter.router.getStateForAction(MainRouter.router.getActionForPathAndParams('Main'));

export const navReducer = (state = initialState, action) => {
    const nextState = MainRouter.router.getStateForAction(action, state);
    return nextState || state;
};
