import React, {Component} from 'react';
import {
    View,
    StyleSheet, Text
} from 'react-native';

export default class DetailScreen extends Component {

    render() {
        const {item} = this.props.navigation.state.params;
        return (
            <View style={styles.container}>
                <Text style={{fontSize: 30}}>{item.name}</Text>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

