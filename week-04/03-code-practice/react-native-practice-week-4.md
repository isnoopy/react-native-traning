# React Native Practice Week 4

<img src="./image/logo.png" width="100px"/>

## Ứng dụng theo dõi giá Crypto Currency (tiếp theo)

- [ ] Thêm tính năng sắp xếp thứ tự coin trong danh sách.
- [ ] Thêm tab chứa danh sách coin yêu thích.
- [ ] Làm chức năng lưu danh sách coin yêu thích, xoá khỏi danh sách yêu thích, sắp xếp coin yêu thích.
