# Week 4
### Roadmap
 - AsyncStorage
    - save data with `key : value`
    - remove data
    - get data
 - Cache Image
 - Realm Database
    - Setup
    - Create schema
    - DAO
 - Firebase
    - Setup
    - Connect firebase

#### 1.AsyncStorage

 - `save`, `get`, `remove` data:

<img src="./image/img-store.png" width="600px"/>

 - Use in component:

 <img src="./image/img-store-use.png" width="600px"/>

#### 2. Cache image

 - Cache Control (iOS Only):

```js
<Image
  source={{
    uri: 'https://facebook.github.io/react/logo-og.png',
    cache: 'only-if-cached',
  }}
  style={{width: 400, height: 400}}
/>
```

`cache`: ['default', 'reload', 'force-cache', 'only-if-cached']

See [docs](https://facebook.github.io/react-native/docs/images.html#cache-control-ios-only)

 * *Trong `Android` tự động cache Image.*

#### 3. Realm database

 - Setup:
 ```sh
 npm install --save realm
 ```

 ```sh
 react-native link realm
 ```

 - Create `Schema` User:

 <img src="./image/img-user-schema.png" width="600px"/>

 - Setup Realm:

 <img src="./image/img-setup-realm.png" width="600px"/>

 - `DAO (Data Access Object Pattern)`:

 <img src="./image/img-dao.png" width="600px"/>

 - [Migration] (https://realm.io/docs/javascript/latest/#migrations) 