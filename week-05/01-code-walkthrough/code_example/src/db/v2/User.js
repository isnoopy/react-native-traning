export class User {

}

User.schema = {
    name: 'User',
    properties: {
        user_id: {type: 'int'},
        username: {type: 'string'},
        email: {type: 'string'}
    }
};