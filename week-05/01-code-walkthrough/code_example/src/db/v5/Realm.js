import Realm from 'src/db/v1/Realm';
import {User} from "./User";
import {Coin} from "./Coin";

const schemas = [
    {schema: [User, Coin], schemaVersion: 0}
];

export const realm = new Realm(schemas[0]);