import BaseDao from "./BaseDao";

export default class CoinDao extends BaseDao {
    constructor() {
        super('Coin');
    }
}