export class CoinV2 {

}

CoinV2.schema = {
    name: 'Coin',
    properties: {
        id: {type: 'string'},
        name: {type: 'string'},
        price_usd: {type: 'string'}
    }
};