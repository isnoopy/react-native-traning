export class UserV2 {

}

UserV2.schema = {
    name: 'User',
    properties: {
        name: {type: 'string'},
        age: {type: 'int'},
        email: {type: 'string'}
    }
};