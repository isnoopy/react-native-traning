import {createStackNavigator} from 'react-navigation';
import ReduxDemo from "../screen/ReduxDemo";
import DetailScreen from "../screen/detail/DetailScreen";
import NewsDetail from "../screen/news/NewsDetail";
import DbDemoScreen from "../screen/db_demo/DbDemoScreen";
import FirebaseScreen from "../screen/firebase/FirebaseScreen";
import Socket from "../screen/socket/Socket";

export const MainRouter = createStackNavigator({
    Main: {
        screen: ReduxDemo
    },
    Detail: {
        screen: DetailScreen
    },
    NewsDetail: {
        screen: NewsDetail
    },
    DB: {
        screen: DbDemoScreen
    },
    Firebase: {
        screen: FirebaseScreen
    },
    Socket: {
        screen: Socket
    }
});