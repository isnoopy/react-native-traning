import React, {Component} from 'react';
import {
    View,
    StyleSheet, Text
} from 'react-native';

import firebase from 'react-native-firebase';

export default class FirebaseScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            token: ''
        }
    }

    async componentDidMount() {
        const token = await firebase.messaging().getToken();
        if (token) {
            // user has a device token
            this.setState({token});
        } else {
            // user doesn't have a device token yet
            this.setState({token: 'token null'});
        }
        await this.checkPermission();
        this.listenNotification();
    }

    checkPermission = async () => {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            // user has permissions
            alert('has permission');
        } else {
            // user doesn't have permission
            alert('no permission');
            await firebase.messaging().requestPermission();
        }
    };

    listenNotification = () => {
        this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
        });
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            const localNotification = new firebase.notifications.Notification()
                .setNotificationId(notification.notificationId)
                .setTitle(notification.title)
                .setBody(notification.body)
                .setData(notification.data).ios.setBadge(notification.ios.badge)
                .android.setChannelId('channelId')
                .android.setSmallIcon('ic_launcher');
            firebase.notifications().displayNotification(localNotification);
            // Process your notification as required
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <Text>{this.state.token}</Text>
            </View>
        );
    }

    componentWillUnmount() {
        this.notificationDisplayedListener();
        this.notificationListener();
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

