import React, {Component} from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import SocketUtil from "../../socket/socket";
import io from 'socket.io-client';

export default class Socket extends Component {


    componentDidMount() {
        this.socket = io('http://localhost:3000');
        this.socket.emit('message', 'data');
    }

    render() {
        return (
            <View style={styles.container}>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

