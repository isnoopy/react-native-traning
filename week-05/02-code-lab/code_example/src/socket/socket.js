import io from "socket.io-client";

const HOST_URL = 'http://localhost:3000';

export default class SocketUtil {

    static async connect() {
        if (this.socket) {
            this.socket.disconnect();
            this.socket = null;
        }
        this.socket = io(HOST_URL);
    }

    static on(key: string, listener) {
        if (!this.socket.hasListeners(listener)) {
            this.socket.on(key, listener);
        }
    }

    static off(key: string, listener) {
        this.socket.off(key, listener);
    }

    static emit(key: string, data) {
        this.socket.emit(key, data);
    }
}
