# Week 5

## Roadmap

 - Ứng dụng Realm database, Migration.
 - Firebase
 	- Setup library
 	- Setup notification
 	- Create firebase project
 	- Send and receive notification
 - Socket IO
 	- Setup
 	- Connect socket

## Realm database migration

 - Tạo thêm version cho Model

 <img src="./image/migration_model.png" width="300px"/>

 - Thêm vào list schema

 <img src="./image/list_schema.png" width="600px"/>

  - Tạo function migration

 <img src="./image/migration_func.png" width="600px"/>

  - Gọi hàm migration

 <img src="./image/migration.png" width="600px"/>

## Firebase

 - Tạo firebase project

 <img src="./image/fb_create.png" width="600px"/>

 - Tạo dự án iOS

  <img src="./image/fb_create_ios.png" width="600px"/>

 - Tạo dự án Android

  <img src="./image/fb_create_android.png" width="600px"/>

 - Lấy mã SHA1:

 `Mac/linux`:

 ```sh
keytool -exportcert -list -v -alias androiddebugkey -keystore ~/.android/debug.keystore
 ```

 `Windows`:

 ```sh
keytool -exportcert -list -v -alias androiddebugkey -keystore %USERPROFILE%\.android\debug.keystore
 ```
  <img src="./image/fb_get_sha1.png" width="600px"/>


 - Thêm firebase vào dự án React-native

 [React native firebase] (https://rnfirebase.io/docs/v5.x.x/getting-started)
