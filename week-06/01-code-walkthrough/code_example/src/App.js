import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import SocketScreen from "./screen/SocketScreen";

type Props = {};
export default class App extends Component<Props> {
    render() {
        return (
            <SocketScreen/>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
