import React, {Component} from 'react';

import {
    View,
    StyleSheet,
    Animated,
    Easing
} from 'react-native';

export default class AnimationView extends Component {

    constructor(props) {
        super(props);
        this.animatedValue = new Animated.Value(0);
    }

    componentDidMount() {
        this.animate();
    }

    animate = () => {
        this.animatedValue.setValue(0);
        Animated.timing(
            this.animatedValue,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.linear
            }
        ).start(() => this.animate())
    };

    render() {
        this.rotate = this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        });

        const marginLeft = this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 300]
        });
        const opacity = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0, 1, 0]
        });
        const moving = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0, 300, 0]
        });

        const rotateX = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: ['0deg', '180deg', '0deg']
        });

        return (
            <View style={styles.container}>
                {/*{this.renderAnimationView({transform: [{this.rotate}]})}*/}
                {this.renderAnimationView({transform: [{rotateX}]})}
                {this.renderAnimationView({marginLeft})}
                {this.renderAnimationView({opacity})}
                {this.renderAnimationView({marginLeft: moving})}
            </View>
        );
    }

    renderAnimationView = (animationStyle) => {
        return (
            <Animated.Image
                style={{
                    width: 100,
                    height: 100,
                    margin: 10,
                    // ...animationStyle
                    transform: [{rotate: this.rotate}]
                }}
                source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png'}}
            />
        );
    };
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    }
});


