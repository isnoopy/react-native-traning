import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    WebView
} from 'react-native';

export default class RNWebView extends Component {

    render() {
        return (
            <View style={styles.container}>
                <WebView
                    source={require('./index.html')}
                    onMessage={this.onStateChange}
                />
            </View>
        );
    }

    onStateChange = (event) => {
        alert('Message: ' + event.nativeEvent.data);
    };
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});


