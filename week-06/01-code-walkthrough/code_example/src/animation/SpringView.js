import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Animated,
    TouchableOpacity,
    Text
} from 'react-native';

export default class SpringView extends Component {

    constructor(props) {
        super(props);
        this.animatedValue = new Animated.Value(0.3);
    }

    animate = () => {
        this.animatedValue.setValue(0.3);
        Animated.spring(
            this.animatedValue,
            {
                toValue: 1,
                friction: 1
            }
        ).start()
    };

    render() {
        const marginLeft = this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 300]
        });

        const moving = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0, 300, 0]
        });

        return (
            <View style={styles.container}>
                {this.renderAnimationView({marginLeft})}
                {this.renderAnimationView({
                    transform: [{scale: this.animatedValue}]
                })}
                {this.renderAnimationView({marginLeft: moving})}

                <TouchableOpacity
                    style={styles.button}
                    onPress={this.onStart}
                >
                    <Text> Start </Text>
                </TouchableOpacity>
            </View>
        );
    }

    onStart = () => {
        this.animate();
    };

    renderAnimationView = (animationStyle) => {
        return (
            <Animated.Image
                style={{
                    width: 100,
                    height: 100,
                    margin: 10,
                    ...animationStyle
                }}
                source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png'}}
            />
        );
    };
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    button: {
        width: '100%',
        height: 50,
        backgroundColor: '#cccccc',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    }
});


