import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text
} from 'react-native';
import SocketIO from 'socket.io-client';

export default class SocketScreen extends Component {

    componentDidMount() {
        this.socket = SocketIO('http://localhost:3000');
        this.socket.on('message', (status) => {
            alert(status);
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    onPress={() => {
                        this.socket.emit('message', 'This is a message!');
                    }}
                >
                    <Text>Send message</Text>
                </TouchableOpacity>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

