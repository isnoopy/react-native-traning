import React, {Component} from 'react';
import ColorProvider from "./src/context/ColorProvider";
import LanguageScreen from "./src/language/LanguageScreen";

export default class App extends Component {
    render() {
        return (
            <ColorProvider>
                <LanguageScreen/>
            </ColorProvider>
        );
    }
}
