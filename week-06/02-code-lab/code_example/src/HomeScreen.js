import React, {Component} from 'react';
import {
    View,
    StyleSheet, Text, TouchableOpacity
} from 'react-native';
import {AppContext} from './AppContext'

export default class HomeScreen extends Component {

    render() {
        return (
            <AppContext.Consumer>
                {(context) => {
                    return (
                        <View style={styles.container}>
                            <Text>{context.value}</Text>
                            <TouchableOpacity onPress={context.change}>
                                <Text>Change</Text>
                            </TouchableOpacity>
                        </View>
                    );

                }}
            </AppContext.Consumer>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#cc532c',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

