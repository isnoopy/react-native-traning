import React, {Component} from 'react';
import {
    View,
    StyleSheet, TouchableOpacity, Text
} from 'react-native';
import ColorConsumer from "./ColorConsumer";

export default class ColorButton extends Component {

    render() {
        return (
            <ColorConsumer
                renderContent={this.renderView}
            />
        );
    }

    renderView = (context) => {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={[styles.button, styles.green]}
                    onPress={() => context.setColor('green')}
                >
                    <Text style={styles.text}>Green</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.button, styles.blue]}
                    onPress={() => context.setColor('blue')}
                >
                    <Text style={styles.text}>Blue</Text>
                </TouchableOpacity>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        width: '80%',
        flexDirection: 'row',
        marginTop: 200
    },
    button: {
        flex: 1,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 20
    },
    green: {
        backgroundColor: 'green'
    },
    blue: {
        backgroundColor: 'blue'
    }
});

