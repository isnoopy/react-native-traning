import React, {Component} from 'react';
import {ColorContext} from './ColorContext'
import PropTypes from 'prop-types';

export default class ColorConsumer extends Component {


    render() {
        const {renderContent} = this.props;
        return (
            <ColorContext.Consumer>
                {(context) => {
                    return renderContent(context);
                }}
            </ColorContext.Consumer>
        );
    }
}

ColorConsumer.propTypes = {
    renderContent: PropTypes.func.isRequired
};


