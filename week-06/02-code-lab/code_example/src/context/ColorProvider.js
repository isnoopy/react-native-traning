import React, {Component} from 'react';
import {ColorContext} from './ColorContext'

export default class ColorProvider extends Component {

    state = {
        color: 'blue',
        setColor: (color) => {
            this.setState({color});
        }
    };

    render() {
        return (
            <ColorContext.Provider
                value={this.state}
            >
                {this.props.children}
            </ColorContext.Provider>
        );
    }
}

