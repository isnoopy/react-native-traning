import React, {Component} from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import ColorView from "./ColorView";
import ColorButton from "./ColorButton";

export default class ColorScreen extends Component {

    render() {
        return (
            <View style={styles.container}>
                <ColorView/>
                <ColorButton/>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

