import React, {Component} from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import ColorConsumer from "./ColorConsumer";

export default class ColorView extends Component {

    render() {
        return (
            <ColorConsumer
                renderContent={this.renderView}
            />
        );
    }

    renderView = (context) => {
        const backgroundColor = context.color;
        return (
            <View style={[styles.container, {backgroundColor}]}/>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '50%',
        height: 100,
    }
});

ColorView.propTypes = {};

