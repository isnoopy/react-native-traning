import React, {Component} from 'react';
import {
    View,
    StyleSheet, Text
} from 'react-native';

import I18n, {getLanguages} from 'react-native-i18n';

I18n.fallbacks = false;

I18n.translations = {
    'en': require('./translations/en'),
    'vi-VN': require('./translations/vi'),
};

export default class LanguageScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            languages: null
        }
    }

    componentDidMount(): void {
        getLanguages().then(languages => {
            this.setState({languages})
        });
        I18n.locale = 'vi-VN';
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={{fontSize: 25}}>{JSON.stringify(this.state.languages)}</Text>
                <Text style={{fontSize: 25}}>{I18n.t('hello')}</Text>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

