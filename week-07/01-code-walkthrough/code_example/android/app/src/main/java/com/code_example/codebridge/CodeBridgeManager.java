//  Created by react-native-create-bridge

package com.code_example.codebridge;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;

import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.views.text.ReactTextView;

public class CodeBridgeManager extends SimpleViewManager<ProgressBar> {
    public static final String REACT_CLASS = "CodeBridge";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public ProgressBar createViewInstance(ThemedReactContext context) {
        return new ProgressBar(context);
    }

    @ReactProp(name = "progress", defaultInt = 0)
    public void setProgress(ProgressBar view, int progress) {
        view.setProgress(progress);
    }

    @ReactProp(name = "indeterminate", defaultBoolean = false)
    public void setIndeterminate(ProgressBar view, boolean indeterminate) {
        view.setIndeterminate(indeterminate);
    }

    @ReactProp(name = "exampleProp")
    public void setExampleProp(View view, String prop) {
    }
}
