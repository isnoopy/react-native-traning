//  Created by react-native-create-bridge

package com.code_example.uitext;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;

import com.facebook.react.uimanager.annotations.ReactProp;

public class UITextManager extends SimpleViewManager<CustomText> {
    public static final String REACT_CLASS = "UIText";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public CustomText createViewInstance(ThemedReactContext context) {
        CustomText view = new CustomText(context);
        view.setText("TextView native module");
        view.setTextColor(Color.BLUE);
        view.setTextSize(30f);
        return view;
    }

    @ReactProp(name = "text")
    public void setText(View view, String prop) {
        ((TextView) view).setText(prop);
    }

    @ReactProp(name = "textSize")
    public void setTextSize(View view, float prop) {
        ((TextView) view).setTextSize(prop);
    }

    @ReactProp(name = "textColor")
    public void setTextColor(View view, String prop) {
        //"0xff0000"
        ((TextView) view).setTextColor(Color.parseColor(prop));
    }

}
