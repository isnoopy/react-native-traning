//  Created by react-native-create-bridge

package com.code_example.uitext;

import android.support.annotation.Nullable;
import android.widget.Toast;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.HashMap;
import java.util.Map;

public class UITextModule extends ReactContextBaseJavaModule {
    public static final String REACT_CLASS = "UIText";
    private static ReactApplicationContext reactContext = null;

    public UITextModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        return constants;
    }

    @ReactMethod
    public void exampleMethod() {
        Toast.makeText(reactContext, "Call method", Toast.LENGTH_LONG).show();
    }

    @ReactMethod
    public void getText(int type, Promise promise) {
        promise.resolve("GenbaCoder" + type);
    }

    @ReactMethod
    public void pushMessage() {
        WritableMap map = Arguments.createMap();
        map.putString("message", "React native");
        emitEvent("push_message", map);
    }

    private static void emitEvent(String eventName, @Nullable WritableMap eventData) {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, eventData);
    }
}
