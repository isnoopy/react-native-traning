import React, {Component} from 'react';
import {StyleSheet, View, TouchableOpacity, Text} from 'react-native';
import UITextView from "./ui_text/UITextNativeView";
import UITextNativeModule from "./ui_text/UITextNativeModule";

type Props = {};
export default class App extends Component<Props> {

    render() {
        return (
            <View style={styles.container}>
                <UITextView
                    style={{
                        width: 400,
                        height: 100
                    }}
                    text={'GenbaCoder.com'}
                    textSize={50}
                />

                <TouchableOpacity
                    onPress={this.sendEvent}
                >
                    <Text style={{fontSize: 20}}>Call method</Text>
                </TouchableOpacity>
            </View>
        );
    }

    componentDidMount(): void {
        UITextNativeModule.emitter.addListener('push_message', (message) => {
            alert(JSON.stringify(message));
        })
    }

    onCallMethod = () => {
        UITextNativeModule.getText(1).then(data => {
            alert(data);
        }).catch(error => {
            console.log(error);
        });
    };

    sendEvent = () => {
        UITextNativeModule.pushMessage();
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});
