//  Created by react-native-create-bridge

import {NativeModules, NativeEventEmitter} from 'react-native'

const {CodeBridge} = NativeModules;
const CodeBridgeEmitter = new NativeEventEmitter(CodeBridge);

export default {
    exampleMethod() {
        return CodeBridge.exampleMethod()
    },
    emitter: CodeBridgeEmitter

}
