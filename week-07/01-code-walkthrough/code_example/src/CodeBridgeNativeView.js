//  Created by react-native-create-bridge

import React, { Component } from 'react'
import { requireNativeComponent } from 'react-native'

const CodeBridge = requireNativeComponent('CodeBridge', CodeBridgeView)

export default class CodeBridgeView extends Component {
  render () {
    return <CodeBridge {...this.props} />
  }
}

CodeBridgeView.propTypes = {
  // exampleProp: React.PropTypes.any
};
