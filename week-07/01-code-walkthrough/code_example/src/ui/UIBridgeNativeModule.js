//  Created by react-native-create-bridge

import { NativeModules } from 'react-native'

const { UIBridge } = NativeModules

export default {
  exampleMethod () {
    return UIBridge.exampleMethod()
  },

  // EXAMPLE_CONSTANT: UIBridge.EXAMPLE_CONSTANT
}
