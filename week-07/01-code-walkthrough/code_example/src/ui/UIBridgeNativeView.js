//  Created by react-native-create-bridge

import React, { Component } from 'react'
import { requireNativeComponent } from 'react-native'

const UIBridge = requireNativeComponent('UIBridge', UIBridgeView)

export default class UIBridgeView extends Component {
  render () {
    return <UIBridge {...this.props} />
  }
}

UIBridgeView.propTypes = {
  // exampleProp: React.PropTypes.any
}
