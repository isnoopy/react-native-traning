//  Created by react-native-create-bridge

import {NativeModules, NativeEventEmitter} from 'react-native'

const {UIText} = NativeModules;
const UITextEmitter = new NativeEventEmitter();

export default {
    exampleMethod() {
        return UIText.exampleMethod()
    },
    getText(type) {
        return UIText.getText(type);
    },
    pushMessage() {
        return UIText.pushMessage();
    },
    emitter: UITextEmitter
}
