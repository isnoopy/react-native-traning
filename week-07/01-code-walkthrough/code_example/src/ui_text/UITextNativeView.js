import React, {Component} from 'react'
import {requireNativeComponent} from 'react-native'
import PropTypes from 'prop-types';

const UIText = requireNativeComponent('UIText', UITextView)

export default class UITextView extends Component {
    render() {
        return <UIText {...this.props} />
    }
}

UITextView.propTypes = {
    text: PropTypes.string,
    textSize: PropTypes.number,
};
